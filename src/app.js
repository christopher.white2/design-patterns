const express = require('express');
const app = express();
const adapt = require('./expressAdapter');
const users = require('./users');

app.get('/', adapt(users.handleGetUsers));
app.get('/:user', adapt(users.handleGetUser))


module.exports = {
  start: (port = 3000) => {
    app.listen(port, () => {
      console.log(`Application listening on port ${port}`);
    });
  }
};
